package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"sort"
	"strconv"
	"sync"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
	"github.com/spf13/afero"
	"gitlab.com/graceharbor/directory-builder/internal"
	"gitlab.com/graceharbor/directory-builder/internal/config"
	"golang.org/x/exp/maps"
)

var directory = binding.NewString()

var fonts map[string]struct{}

func extract(ctx context.Context) error {
	cwd, err := directory.Get()
	if err != nil {
		return err
	}
	if cwd == "" {
		return fmt.Errorf("invalid directory")
	}
	err = internal.Extract(ctx, cwd)
	if err == nil {
		fonts, err = internal.GetFonts(ctx, cwd)
	}
	return err
}
func retrieve(ctx context.Context, p *fyneProgress) error {
	cwd, err := directory.Get()
	if err != nil {
		return err
	}
	if cwd == "" {
		return fmt.Errorf("invalid directory")
	}
	prog := &internal.Progress{}
	var wg sync.WaitGroup
	wg.Add(1)
	defer wg.Wait()
	var finished = make(chan struct{})
	defer close(finished)
	go func() {
		defer wg.Done()
		var ticker = time.NewTicker(time.Millisecond * 50)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				p.Set(int(prog.Total.Load()), int(prog.Done.Load()))
			case <-ctx.Done():
				return
			case <-finished:
				return
			}
		}
	}()
	return internal.Retrieve(afero.NewOsFs(), ctx, cwd, prog)
}
func build(ctx context.Context) error {
	cwd, err := directory.Get()
	if err != nil {
		return err
	}
	if cwd == "" {
		return fmt.Errorf("invalid directory")
	}
	err = internal.Build(ctx, cwd)
	return err
}
func impose(ctx context.Context) error {
	cwd, err := directory.Get()
	if err != nil {
		return err
	}
	if cwd == "" {
		return fmt.Errorf("invalid directory")
	}
	err = internal.Impose(ctx, cwd)
	return err
}

type operationStatus int

const opOK = 0
const opRUNNING = 1
const opOUTDATED = 2
const opKO = 3

type fyneProgress struct {
	prog    *widget.ProgressBar
	progInf *widget.ProgressBarInfinite

	Container *fyne.Container

	value binding.Float
}

func (f *fyneProgress) Set(total, value int) {
	if total == 0 {
		f.progInf.Start()
		f.prog.Hidden = true
		f.progInf.Hidden = false
	} else {
		f.value.Set(float64(value) / float64(total))
		f.progInf.Hidden = true
		f.prog.Hidden = false
		f.progInf.Stop()
	}
}

func newProgress() *fyneProgress {
	ret := &fyneProgress{}
	ret.value = binding.NewFloat()
	ret.prog = widget.NewProgressBarWithData(ret.value)
	ret.progInf = widget.NewProgressBarInfinite()
	ret.Container = container.NewGridWithColumns(1, ret.prog, ret.progInf)
	return ret
}

type operationWidget struct {
	but     *widget.Button
	icon    *widget.Icon
	iconBut *widget.Button
	label   *widget.Label

	editBut *widget.Button

	labelGroup *fyne.Container

	container *fyne.Container

	status operationStatus

	f        func(ctx context.Context) error
	f2       func(ctx context.Context, progress *fyneProgress) error
	progress *fyneProgress

	parent     *operationWidget
	dependents []*operationWidget

	ctx       context.Context
	ctxCancel context.CancelFunc
	ctxOnce   *sync.Once
}

type configField struct {
	Label    string
	Modified bool
	Value    any

	Container *fyne.Container
	Fields    []*configField
}
type configContainer struct {
	Container *fyne.Container

	Value  any
	Fields []*configField
}

func buildWidget(value reflect.Value, i int) *configField {
	field := value.Field(i)
	fieldType := value.Type().Field(i)

	if !field.CanInterface() {
		return nil
	}

	label := fieldType.Tag.Get("label")
	if label == "" {
		return nil
	}
	var ret = &configField{label, false, nil, nil, nil}
	labelWidget := widget.NewLabel(label)

	var inputWidget fyne.CanvasObject

	switch field.Kind() {
	case reflect.Pointer:
	case reflect.Struct:
		ret.Value = field
		cont := container.NewVBox()
		ret.Container = container.NewVBox(labelWidget, cont)
		for j := 0; j < field.NumField(); j++ {
			if w := buildWidget(field, j); w != nil {
				ret.Fields = append(ret.Fields, w)
				cont.Add(container.New(layout.NewPaddedLayout(), w.Container))
			}
		}
		if len(cont.Objects) == 0 {
			return nil
		}
		return ret
	case reflect.String:
		ret.Value = field.Addr().Interface().(*string)
		inputWidget = widget.NewEntry()
		inputWidget.(*widget.Entry).SetText(field.String())
		inputWidget.(*widget.Entry).OnChanged = func(s string) {
			ret.Modified = true
			*(ret.Value.(*string)) = s
		}

	case reflect.Int:
		ret.Value = field.Addr().Interface().(*int)
		inputWidget = widget.NewEntry()
		inputWidget.(*widget.Entry).SetText(fmt.Sprintf("%d", field.Int()))
		inputWidget.(*widget.Entry).Validator = func(s string) error {
			_, err := strconv.Atoi(s)
			return err
		}
		inputWidget.(*widget.Entry).OnChanged = func(s string) {
			ret.Modified = true
			*(ret.Value.(*int)), _ = strconv.Atoi(s)
		}
	}

	if inputWidget != nil {
		ret.Container = container.NewGridWithColumns(2, labelWidget, inputWidget)
		return ret
	}
	return nil
}
func buildWidgets(value any) *configContainer {
	valueOf := reflect.ValueOf(value).Elem()

	ret := &configContainer{container.NewVBox(), value, nil}

	for i := 0; i < valueOf.NumField(); i++ {
		if w := buildWidget(valueOf, i); w != nil {
			ret.Fields = append(ret.Fields, w)
			ret.Container.Add(w.Container)
		}
	}

	return ret
}

func newOperationEditWidget(name string, f func(context.Context) error, edit func(context.Context) (modified bool, err error), parent *operationWidget) *operationWidget {
	return _newOperationWidget(name, f, nil, edit, parent)
}
func newOperationWidget(name string, f func(context.Context) error, parent *operationWidget) *operationWidget {
	return _newOperationWidget(name, f, nil, nil, parent)
}
func newOperationWidgetProgress(name string, f func(context.Context, *fyneProgress) error, parent *operationWidget) *operationWidget {
	return _newOperationWidget(name, nil, f, nil, parent)
}
func _newOperationWidget(name string, f func(context.Context) error, f2 func(context.Context, *fyneProgress) error, edit func(context.Context) (modified bool, err error), parent *operationWidget) *operationWidget {
	status := widget.NewLabelWithStyle("init", fyne.TextAlignLeading, fyne.TextStyle{Bold: true})
	icon := widget.NewIcon(theme.ViewRefreshIcon())
	iconBut := widget.NewButtonWithIcon("", theme.ViewRefreshIcon(), func() {})

	prog := newProgress()
	label := container.New(layout.NewGridLayout(2), icon, iconBut, status, prog.Container)
	runBut := widget.NewButton(name, func() {})
	runBut.Resize(fyne.NewSize(300, 0))
	widg := &operationWidget{
		runBut,
		icon,
		iconBut,
		status,
		nil,
		label,
		container.New(
			layout.NewGridLayout(2),
			runBut,
			label,
		),
		0,
		f,
		f2,
		prog,
		parent,
		nil,
		context.Background(),
		nil,
		nil,
	}
	if widg.parent != nil {
		widg.parent.AddDependent(widg)
	}
	if edit != nil {
		runBut.Resize(fyne.NewSize(200, 0))
		widg.editBut = widget.NewButtonWithIcon("", theme.MailComposeIcon(), func() {
			go func() {
				widg.ctx, widg.ctxCancel = context.WithCancel(context.TODO())
				defer widg.ctxCancel()
				var modified, err = edit(widg.ctx)
				if err != nil {
					widg.SetStatus(opKO)
				} else if modified {
					widg.SetStatus(opOUTDATED)
				} else {
					widg.SetStatus(opOK)
				}
			}()
		})
		widg.editBut.Resize(fyne.NewSize(100, 0))
		widg.container = container.New(
			layout.NewGridLayout(2),
			container.New(layout.NewGridLayout(2), runBut, widg.editBut),
			label,
		)
	}
	widg.SetStatus(opOUTDATED)
	runBut.OnTapped = widg.Run
	iconBut.OnTapped = widg.cancel
	return widg
}

func (o *operationWidget) AddDependent(d *operationWidget) {
	o.dependents = append(o.dependents, d)
}
func (o *operationWidget) Disable() { o.but.Disable() }
func (o *operationWidget) Enable()  { o.but.Enable() }
func (o *operationWidget) cancel() {
	o.ctxOnce.Do(func() {
		fmt.Println("cancelled")
		o.ctxCancel()
	})
}
func (o *operationWidget) Run() {
	o.SetStatus(opRUNNING)
	o.ctx, o.ctxCancel = context.WithCancel(context.TODO())
	o.ctxOnce = &sync.Once{}
	fmt.Printf("running: %s\n", o.but.Text)
	go func() {
		defer func() {
			o.ctxOnce.Do(o.ctxCancel)
			o.ctxOnce = nil
		}()

		var err error
		if o.f == nil {
			err = o.f2(o.ctx, o.progress)
		} else {
			err = o.f(o.ctx)
		}
		fmt.Printf("  finished: %s (err: %s)\n", o.but.Text, fmt.Sprint(err != nil))
		if err != nil {
			o.SetStatus(opKO)
		} else {
			o.SetStatus(opOK)
		}
	}()
}
func (o *operationWidget) Container() *fyne.Container { return o.container }
func (o *operationWidget) setIcon(text string, isButton bool, resource fyne.Resource) {
	o.label.SetText(text)
	o.icon.Hidden = isButton
	o.iconBut.Hidden = !isButton
	if isButton {
		o.iconBut.SetIcon(resource)
	} else {
		o.icon.SetResource(resource)
	}
}
func (o *operationWidget) Edit() {
}
func (o *operationWidget) SetStatus(op operationStatus) {
	o.status = op
	switch op {
	case opOK:
		o.setIcon("OK", false, theme.ConfirmIcon())
	case opRUNNING:
		o.setIcon("running", true, theme.CancelIcon())
		o.progress.Set(0, 0)
		o.label.Hidden = true
		o.progress.Container.Hidden = false
	case opOUTDATED:
		o.setIcon("out of date", false, theme.MediaReplayIcon())
	case opKO:
		o.setIcon("failed", false, theme.WarningIcon())
	}
	if o.status != opOK {
		for _, d := range o.dependents {
			d.SetStatus(opOUTDATED)
			d.Disable()
		}
	} else {
		for _, d := range o.dependents {
			d.Enable()
		}
	}
	if o.status == opRUNNING {

		parent := o.parent
		for parent != nil {
			parent.Disable()
			parent = parent.parent
		}
		for _, d := range o.dependents {
			d.SetStatus(opOUTDATED)
			d.Disable()
		}
	} else {
		o.progress.Container.Hidden = true
		o.label.Hidden = false
		parent := o.parent
		for parent != nil {
			parent.Enable()
			parent = parent.parent
		}
	}
}

func main() {
	fs := afero.NewOsFs()
	a := app.New()
	a.Settings().SetTheme(theme.LightTheme())
	w := a.NewWindow("GHC Directory Builder")

	cwd, cwdErr := os.Getwd()
	if cwdErr == nil {
		gitignoreStat, gitignoreErr := os.Stat(filepath.Join(cwd, ".gitignore"))
		buildDirStat, buildDirErr := os.Stat(filepath.Join(cwd, "build"))
		if gitignoreErr == nil && !gitignoreStat.IsDir() && buildDirErr == nil && buildDirStat.IsDir() {
			cwd = filepath.Join(cwd, "build")
		}
		directory.Set(cwd)
	}
	sep := widget.NewSeparator()

	extractWidget := newOperationWidget("Extract tools", extract, nil)
	var pcoConfigWidget *operationWidget
	pcoConfigWidget = newOperationEditWidget(
		"PCO Config",
		func(ctx context.Context) (err error) {
			cwd, err := directory.Get()
			if err != nil {
				return
			}
			_, err = config.LoadConfig(fs, cwd)
			if err != nil {
				return
			}
			return
		},
		func(ctx context.Context) (modified bool, err error) {
			cwd, err := directory.Get()
			if err != nil {
				return
			}
			conf, err := config.LoadConfig(fs, cwd)
			if err != nil {
				return
			}
			new_ctx, cancelf := context.WithCancel(ctx)
			var canceled bool
			defer func() {
				if err == nil && !canceled {
					modified = true
					err = config.DumpConfig(fs, cwd, conf)
				}
			}()
			old := w.Content()
			defer func() {
				w.SetContent(old)
			}()

			cancel := widget.NewButton("Cancel", func() {
				canceled = true
				cancelf()
			})
			cancel.Enable()
			save := widget.NewButton("Save", func() {
				cancelf()
			})
			if conf.OutOfArea == nil {
				conf.OutOfArea = &struct {
					DataLabel string "yaml:\"data-label\" label:\"PCO data label\""
				}{}
			}
			save.Enable()
			outofarea := widget.NewEntryWithData(binding.BindString(&conf.OutOfArea.DataLabel))
			next := widget.NewEntryWithData(binding.BindString(&conf.Next))
			dates := widget.NewEntryWithData(binding.BindString(&conf.Dates))
			fonts := maps.Keys(fonts)
			sort.Strings(fonts)
			logo := widget.NewLabelWithData(binding.BindString(&conf.Logo))
			logoBut := widget.NewButtonWithIcon("Set logo", theme.FolderOpenIcon(), func() {
				// Display the folder selection dialog
				dialog.ShowFolderOpen(func(uri fyne.ListableURI, err error) {
					if uri == nil || err != nil {
						return
					}
					cwd, err := directory.Get()
					if err != nil {
						return
					}
					rel, err := filepath.Rel(cwd, uri.Path())
					if err != nil {
						return
					}
					conf.Logo = rel
				}, w)
			})
			var fontLabel *widget.Label = widget.NewLabel("Font")
			fontSelector := widget.NewSelect(fonts, func(s string) {
				conf.Style.FontFamily = s
			})
			fontSelector.Selected = conf.Style.FontFamily
			covenant := widget.NewMultiLineEntry()
			covenant.Text = conf.Covenant
			covenant.OnSubmitted = func(s string) {
				conf.Covenant = s
			}
			configuration := container.NewVBox(
				widget.NewCheckWithData("Show builder version", binding.BindBool(&conf.PlaceGitHash)),
				container.New(layout.NewGridLayout(2),
					widget.NewLabel("Out-of-area data label"), outofarea,
					fontLabel, fontSelector,
					widget.NewLabel("Next meeting (shown on back)"), next,
					widget.NewLabel("Dates (shown on front)"), dates,
					logoBut, logo),
				widget.NewLabel("Covenant"),
				covenant,
			)
			covenant.SetMinRowsVisible(20)
			// cc := buildWidgets(&conf)
			scroll := container.NewVScroll(configuration)
			scroll.SetMinSize(fyne.NewSize(10, 400))
			w.SetContent(container.NewVBox(
				widget.NewLabel("Configuration"),
				container.NewHBox(sep),
				container.NewHBox(cancel, save),
				scroll,
			))
			select {
			case <-ctx.Done():
				canceled = true
				err = ctx.Err()
				return
			case <-new_ctx.Done():
			}
			return
		}, extractWidget)
	retrieveWidget := newOperationWidgetProgress("Retrieve data", retrieve, pcoConfigWidget)
	var configWidget *operationWidget
	configWidget = newOperationEditWidget(
		"PDF Config",
		func(ctx context.Context) (err error) {
			cwd, err := directory.Get()
			if err != nil {
				return
			}
			_, err = config.LoadConfig(fs, cwd)
			if err != nil {
				return
			}
			return
		},
		func(ctx context.Context) (modified bool, err error) {
			cwd, err := directory.Get()
			if err != nil {
				return
			}
			conf, err := config.LoadConfig(fs, cwd)
			if err != nil {
				return
			}
			new_ctx, cancelf := context.WithCancel(ctx)
			var canceled bool
			defer func() {
				if err == nil && !canceled {
					modified = true
					err = config.DumpConfig(fs, cwd, conf)
				}
			}()
			old := w.Content()
			defer func() {
				w.SetContent(old)
			}()

			cancel := widget.NewButton("Cancel", func() { canceled = true; cancelf() })
			save := widget.NewButton("Save", func() { cancelf() })
			if conf.OutOfArea == nil {
				conf.OutOfArea = &struct {
					DataLabel string "yaml:\"data-label\" label:\"PCO data label\""
				}{}
			}
			outofarea := widget.NewEntryWithData(binding.BindString(&conf.OutOfArea.DataLabel))
			next := widget.NewEntryWithData(binding.BindString(&conf.Next))
			dates := widget.NewEntryWithData(binding.BindString(&conf.Dates))
			fonts := maps.Keys(fonts)
			sort.Strings(fonts)
			logo := widget.NewLabelWithData(binding.BindString(&conf.Logo))
			logoBut := widget.NewButtonWithIcon("Set logo", theme.FolderOpenIcon(), func() {
				// Display the folder selection dialog
				dialog.ShowFolderOpen(func(uri fyne.ListableURI, err error) {
					if uri == nil || err != nil {
						return
					}
					cwd, err := directory.Get()
					if err != nil {
						return
					}
					rel, err := filepath.Rel(cwd, uri.Path())
					if err != nil {
						return
					}
					conf.Logo = rel
				}, w)
			})
			var fontLabel *widget.Label = widget.NewLabel("Font")
			fontSelector := widget.NewSelect(fonts, func(s string) {
				conf.Style.FontFamily = s
			})
			fontSelector.Selected = conf.Style.FontFamily
			covenant := widget.NewMultiLineEntry()
			covenant.Text = conf.Covenant
			covenant.OnSubmitted = func(s string) {
				conf.Covenant = s
			}
			configuration := container.NewVBox(
				widget.NewCheckWithData("Show builder version", binding.BindBool(&conf.PlaceGitHash)),
				container.New(layout.NewGridLayout(2),
					widget.NewLabel("Out-of-area data label"), outofarea,
					fontLabel, fontSelector,
					widget.NewLabel("Next meeting (shown on back)"), next,
					widget.NewLabel("Dates (shown on front)"), dates,
					logoBut, logo),
				widget.NewLabel("Covenant"),
				covenant,
			)
			covenant.SetMinRowsVisible(20)
			scroll := container.NewVScroll(configuration)
			scroll.SetMinSize(fyne.NewSize(10, 400))
			w.SetContent(container.NewVBox(
				widget.NewLabel("Configuration"),
				container.NewHBox(sep),
				container.NewHBox(cancel, save),
				scroll,
			))
			select {
			case <-ctx.Done():
				canceled = true
				err = ctx.Err()
				return
			case <-new_ctx.Done():
			}
			return
		}, retrieveWidget)
	buildWidget := newOperationWidget("Build basic directory", build, configWidget)
	imposeWidget := newOperationWidget("Impose directory for printing", impose, buildWidget)
	extractWidget.SetStatus(opOUTDATED)

	selectedDirLabel := widget.NewLabel("Selected Directory: ")
	selectedDir := widget.NewLabelWithData(directory)
	selectedDir.TextStyle.Bold = true
	selectDirButton := widget.NewButtonWithIcon("Select Directory", theme.FolderOpenIcon(), func() {
		// Display the folder selection dialog
		dialog.ShowFolderOpen(func(uri fyne.ListableURI, err error) {
			if err == nil && uri != nil {
				directory.Set(filepath.Clean(uri.Path()))
				configWidget.SetStatus(opOUTDATED)
			}
		}, w)
	})

	w.Resize(fyne.NewSize(600, 400))

	hello := widget.NewLabel("Welcome; please select a directory first, then press a button to run an operation")
	// prog := widget.NewProgressBar()
	// proginf := widget.NewProgressBarInfinite()
	// proginf.Stop()
	w.SetContent(container.NewVBox(
		hello,
		container.NewHBox(sep),
		container.NewHBox(selectedDirLabel, selectedDir),
		selectDirButton,
		container.NewHBox(sep),
		extractWidget.Container(),
		pcoConfigWidget.Container(),
		retrieveWidget.Container(),
		configWidget.Container(),
		buildWidget.Container(),
		imposeWidget.Container(),
		layout.NewSpacer(),
		widget.NewButton("Quit", func() {
			a.Quit()
		}),
	))

	w.ShowAndRun()

}

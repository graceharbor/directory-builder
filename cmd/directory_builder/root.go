package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"strings"
	"syscall"

	"github.com/lithammer/dedent"
	"github.com/spf13/afero"
	"github.com/spf13/cobra"
	"gitlab.com/graceharbor/directory-builder/internal"
)

var rootCmd = &cobra.Command{
	Use:   "ghcdir",
	Short: "GHC directory generator",
	Long: strings.TrimLeft(dedent.Dedent(`
		GHC's directory generator.
		Connect to One Church.  Massage the data.  Create the PDF.  Make it print-ready.
		`), "\n"),
}

var runner func(ctx context.Context) error

func init() {
	rootCmd.CompletionOptions.DisableDefaultCmd = true

	var userCwd string
	rootCmd.PersistentFlags().StringVar(&userCwd, "dir", "", "set build directory")

	getwd := func() (string, error) {
		cwd, cwdErr := os.Getwd()
		if userCwd != "" {
			if filepath.IsAbs(userCwd) {
				return userCwd, nil
			}
		}
		if cwdErr != nil {
			return "", cwdErr
		}
		if userCwd == "" {
			return cwd, nil
		}
		return filepath.Join(cwd, userCwd), nil
	}

	var step int
	addStep := func(cmd *cobra.Command) *cobra.Command {
		step++
		stepName := fmt.Sprintf("Step %d", step)
		var g = &cobra.Group{ID: stepName, Title: stepName}
		rootCmd.AddGroup(g)
		cmd.GroupID = stepName
		rootCmd.AddCommand(cmd)
		return cmd
	}
	addStep(&cobra.Command{
		Use:   "config",
		Short: "configure the directory",
		Long: strings.TrimLeft(dedent.Dedent(`
			Configure the current directory for creating a GHC printed members' directory.
			`), "\n"),
		Run: func(cmd *cobra.Command, args []string) {
			runner = func(ctx context.Context) error { fmt.Println("configure"); return nil }
		},
		GroupID: "config",
	})

	var forceExtract bool
	cmd := addStep(&cobra.Command{
		Use:   "extract",
		Short: "extract static data for building",
		Long: strings.TrimLeft(dedent.Dedent(`
			Extracts static data into the current directory for use during the build process.
			`), "\n"),
		RunE: func(cmd *cobra.Command, args []string) error {
			cwd, err := getwd()
			if err != nil {
				return err
			}
			runner = func(ctx context.Context) error { return internal.Extract(ctx, cwd) }
			return nil
		},
	})
	cmd.Flags().BoolVarP(&forceExtract, "force", "f", false, "force overwritting current data")

	addStep(&cobra.Command{
		Use:   "retrieve",
		Short: "get current member data from One Church",
		Long: strings.TrimLeft(dedent.Dedent(`
			Retrieves the current member data from our One Church database.
			`), "\n"),
		RunE: func(cmd *cobra.Command, args []string) error {
			cwd, err := getwd()
			if err != nil {
				return err
			}
			runner = func(ctx context.Context) error { return internal.Retrieve(afero.NewOsFs(), ctx, cwd, nil) }
			return nil
		},
	})

	// addStep(&cobra.Command{
	// 	Use:   "sort",
	// 	Short: "sort downloaded member data",
	// 	Long: strings.TrimLeft(dedent.Dedent(`
	// 		Sorts the downloaded member data to prepare for the document build.
	// 		`), "\n"),
	// 	Run: func(cmd *cobra.Command, args []string) {
	// 		runner = func(ctx context.Context) error { fmt.Println("sort"); return nil }
	// 	},
	// })

	addStep(&cobra.Command{
		Use:   "build",
		Short: "builds the directory as PDF",
		Long: strings.TrimLeft(dedent.Dedent(`
			Builds the PDF directory from sorted data.
			`), "\n"),
		RunE: func(cmd *cobra.Command, args []string) error {
			cwd, err := getwd()
			if err != nil {
				return err
			}
			runner = func(ctx context.Context) error { return internal.Build(ctx, cwd) }
			return nil
		},
	})

	addStep(&cobra.Command{
		Use:   "impose",
		Short: "prepares the built PDF for printing",
		Long: strings.TrimLeft(dedent.Dedent(`
			Prepares the built PDF for printing by running imposition.
			`), "\n"),
		RunE: func(cmd *cobra.Command, args []string) error {
			cwd, err := getwd()
			if err != nil {
				return err
			}
			runner = func(ctx context.Context) error { return internal.Impose(ctx, cwd) }
			return nil
		},
	})

}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		log.Fatalln(err.Error())
	}
	if runner == nil {
		log.Fatalln("internal error: runner is nil")
	}
	ctxTerm, stopTerm := signal.NotifyContext(context.Background(), syscall.SIGTERM)
	defer stopTerm()
	ctxInterrupt, stopInterrupt := signal.NotifyContext(ctxTerm, os.Interrupt)
	defer stopInterrupt()
	err = runner(ctxInterrupt)
	if err != nil {
		log.Println(err.Error())
		os.Exit(2)
	}
}

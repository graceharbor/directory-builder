package internal

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"image/jpeg"
	"image/png"
	"io"
	"math"
	"net/http"
	"os"
	"path/filepath"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/afero"
	"gitlab.com/graceharbor/directory-builder/internal/config"
	"gitlab.com/graceharbor/go-pco"
	pco_typed "gitlab.com/graceharbor/go-pco/typed"
	"golang.org/x/exp/slices"
)

const childRelationship = "Child"

type imageDownload struct {
	URL    string
	Person *directoryPerson
}

const customFields = "/field_definitions"

// convertToPng converts an image to png
func convertToPng(imageBytes []byte) ([]byte, error) {
	contentType := http.DetectContentType(imageBytes)

	switch contentType {
	case "image/png":
	case "image/jpeg":
		img, err := jpeg.Decode(bytes.NewReader(imageBytes))
		if err != nil {
			return nil, errors.Wrap(err, "unable to decode jpeg")
		}

		buf := new(bytes.Buffer)
		if err := png.Encode(buf, img); err != nil {
			return nil, errors.Wrap(err, "unable to encode png")
		}

		return buf.Bytes(), nil
	}

	return nil, fmt.Errorf("unable to convert %#v to png", contentType)
}

func getList(conn *pco.Connection, ctx context.Context, listID int) (name string, count int, people *pco.AllResponses, err error) {
	list, err := conn.Getf(ctx, pco.People, "/lists/%d", listID)
	if err != nil {
		return
	}
	type listData struct {
		Name  string `json:"name"`
		Count int    `json:"total_people"`
	}
	var data listData
	err = list.Datum.As(&data)
	if err != nil {
		return
	}

	var listing = conn.GetAllChanf(ctx, pco.People, "/lists/%d/people?include=field_data,emails,households,phone_numbers,addresses", listID)
	return data.Name, data.Count, listing, err
}

func Retrieve(fs afero.Fs, ctx context.Context, cwd string, prog *Progress) (err error) {
	conf, err := config.LoadConfig(fs, cwd)
	if err != nil {
		return
	}
	if conf.Lists.Members == 0 || conf.Lists.Kids == 0 {
		return fmt.Errorf("invalid configuration of members/kids list names")
	}

	conn := pco.NewConnection(conf.API.App, conf.API.Token)

	var outOfAreaID string
	var memberSinceID string
	var hideAddressID string
	if conf.OutOfArea != nil {
		data := conn.GetAllChan(ctx, pco.People, customFields)
		type FieldDef struct {
			Name string `json:"name"`
			Type string `json:"data_type"`
		}
		for field := range data.Values {
			var fd FieldDef
			if field.As(&fd) == nil && fd.Type == "boolean" && fd.Name == conf.OutOfArea.DataLabel {
				outOfAreaID = field.ID
			}
			if field.As(&fd) == nil && fd.Type == "boolean" && fd.Name == conf.HideAddress.DataLabel {
				hideAddressID = field.ID
			}
			if field.As(&fd) == nil && fd.Type == "date" && fd.Name == conf.MemberSince.DataLabel {
				memberSinceID = field.ID
			}
		}
	}

	name, count, members, err := getList(conn, ctx, conf.Lists.Members)
	if err != nil {
		return
	}
	defer members.Close()
	fmt.Printf("found list %d (%s) with %d people\n", conf.Lists.Members, name, count)
	name, count, kids, err := getList(conn, ctx, conf.Lists.Kids)
	if err != nil {
		return
	}
	defer kids.Close()
	fmt.Printf("found list %d (%s) with %d kids\n", conf.Lists.Kids, name, count)

	total := members.TotalValues()*2 + kids.TotalValues()
	if total == 0 {
		return fmt.Errorf("found no people")
	}
	imgDir := filepath.Join(cwd, "img")
	os.MkdirAll(imgDir, 0o755)

	var personsRetrieved atomic.Int32
	var imgDownloaded atomic.Int32
	var imgSkipped atomic.Int32
	var imgNull atomic.Int32
	var jsonWritten = make(chan bool)
	var jsonOnce sync.Once

	// need 1) person and 2) image and 3) dump json
	var progressWg sync.WaitGroup
	progressWg.Add(1)
	defer progressWg.Wait()
	defer close(jsonWritten)
	defer jsonOnce.Do(func() { jsonWritten <- false })
	go func() {
		defer progressWg.Done()
		fmt.Println("determining amount of data...")
		fmt.Printf("persons: %d\n", members.TotalValues()+kids.TotalValues())

		if prog != nil {
			prog.Total.Store(int32(total))
		}
		totalStrLen := len(strconv.Itoa(total))

		var ticker = time.NewTicker(time.Millisecond * 250)
		var previousCount int32
		for {
			var shouldBreak bool
			var count int32
			select {
			case <-ticker.C:
			case wasWritten := <-jsonWritten:
				shouldBreak = true
				ticker.Stop()
				if wasWritten {
					count++
				}
			}
			count += personsRetrieved.Load() + imgSkipped.Load() + imgDownloaded.Load() + imgNull.Load()
			if count != previousCount || count == 0 {
				frac := math.Floor(float64(count) / float64(total) * 100)
				fmt.Printf("  %*d of %d (%3.0f%%)\r", totalStrLen, count, total, frac)
				previousCount = count
				if prog != nil {
					prog.Done.Store(count)
				}
			}
			if shouldBreak {
				break
			}
		}
		fmt.Println("")
		fmt.Printf("  img skipped:    %d\n", imgSkipped.Load())
		fmt.Printf("  img downloaded: %d\n", imgDownloaded.Load())
		fmt.Printf("using %d API calls\n", conn.TotalCallCount())
	}()

	var imageUrls = make(chan imageDownload, 100)
	var imgEtags = make(chan string, 100)
	var currentImgEtags = map[string]struct{}{}
	var imgErr error
	var imgErrLock sync.Mutex
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		defer wg.Done()
		for etag := range imgEtags {
			currentImgEtags[etag] = struct{}{}
		}
	}()
	go func() {
		defer wg.Done()
		defer close(imgEtags)
		for i := range imageUrls {
			wg.Add(1)
			go func(id imageDownload, ctx context.Context) {
				defer wg.Done()
				var err error
				defer func() {
					if err != nil {
						imgErrLock.Lock()
						defer imgErrLock.Unlock()
						if imgErr == nil {
							imgErr = errors.Wrapf(err, "for person %s", id.Person.DisplayName)
						}
						return
					}
				}()
				imgUrl := *id.Person.Avatar
				req, err := http.NewRequestWithContext(ctx, "HEAD", imgUrl, nil)
				if err != nil {
					return
				}
				response, err := http.DefaultClient.Do(req)
				if err != nil {
					return
				}
				etag := strings.Trim(response.Header.Get("ETag"), "\"")
				if etag == "" {
					err = fmt.Errorf("missing photo ETag for %d", id.Person.ID)
					return
				}
				imgEtags <- etag
				ext := filepath.Ext(imgUrl)
				*id.Person.LocalImage = fmt.Sprintf("img/%s%s", etag, ext)
				imgFile := filepath.Join(cwd, *id.Person.LocalImage)
				_, err = os.Stat(imgFile)
				if err == nil {
					imgSkipped.Add(1)
					return
				}
				if errors.Is(err, os.ErrNotExist) {
					err = nil
				}
				if err != nil {
					return
				}
				req, err = http.NewRequestWithContext(ctx, "GET", id.URL, nil)
				if err != nil {
					return
				}
				response, err = http.DefaultClient.Do(req)
				if err != nil {
					return
				}
				defer response.Body.Close()
				imgData, err := io.ReadAll(response.Body)
				if err != nil {
					return
				}
				// imgData, err = convertToPng(imgData)
				// if err != nil {
				// 	return
				// }
				f, err := os.Create(imgFile)
				if err != nil {
					return
				}
				defer f.Close()
				f.Write(imgData)
				imgDownloaded.Add(1)
			}(i, ctx)
		}
	}()
	defer func() {
		if err == nil {
			err = imgErr
		}
	}()
	defer wg.Wait()
	var closeImg sync.Once
	defer closeImg.Do(func() { close(imageUrls) })

	var dpeople []*directoryPerson
	var households = map[int]*household{}
	addHousehold := func(person *directoryPerson, rel pco.ItemRelationships) {
		relHouseholds, ok := rel["households"]
		if ok {
			var thisHousehold = &household{}
			if len(relHouseholds) > 0 {
				hid, _ := strconv.Atoi(relHouseholds[0].ID)
				relHouseholds[0].As(thisHousehold)
				if _, ok := households[hid]; !ok {
					thisHousehold.ID = hid
					thisHousehold.Name = strings.TrimSuffix(thisHousehold.Name, "Household")
					thisHousehold.Name = strings.TrimSpace(thisHousehold.Name)
					households[hid] = thisHousehold
				}
				person.Household = households[hid]
				households[hid].people = append(households[hid].people, person)
			}
		}
	}
	for p := range members.Values {
		var person pco_typed.Person
		if p.As(&person) != nil {
			return err
		}
		personsRetrieved.Add(1)
		pid, err := strconv.Atoi(person.ID)
		if err != nil {
			return errors.Wrapf(err, "unexpected person ID: %s", person.ID)
		}
		dp := &directoryPerson{
			ID:          pid,
			FamilyName:  person.FamilyName,
			DisplayName: person.Name,
			FirstName:   person.FirstName,
			Nickname:    person.FirstName,
			Gender:      person.Gender,
		}
		if person.GivenName != "" {
			dp.FirstName = person.GivenName
		}
		field_data, ok := p.Relationships["field_data"]
		if ok {
			for _, fd := range field_data {
				if fieldDef, ok := fd.Relationships["field_definition"]; ok {
					if len(fieldDef) > 0 && fieldDef[0].ID == outOfAreaID {
						type FieldBool struct {
							Value string `json:"value"`
						}
						var fdv FieldBool
						fd.As(&fdv)
						dp.OutOfArea = fdv.Value == "true"
					}
					if len(fieldDef) > 0 && fieldDef[0].ID == hideAddressID {
						type FieldBool struct {
							Value string `json:"value"`
						}
						var fdv FieldBool
						fd.As(&fdv)
						if fdv.Value == "true" {
							dp.Address = nil
						}
					}
					if len(fieldDef) > 0 && fieldDef[0].ID == memberSinceID {
						type FieldDate struct {
							Value string `json:"value"`
						}
						var fdv FieldDate
						fd.As(&fdv)
						var d, err = time.Parse("01/02/2006", fdv.Value)
						if err != nil {
							return fmt.Errorf("failed to read datum '%s' ('%s') as date: %w", conf.MemberSince.DataLabel, fdv.Value, err)
						}
						ocd := oneChurchDate{
							Month: int(d.Month()),
							Year:  d.Year(),
							Day:   d.Day(),
						}
						dp.Since = new(float64)
						*dp.Since = ocd.Years()
					}
				}
			}
		}
		emails, ok := p.Relationships["emails"]
		if ok {
			for _, e := range emails {
				var ee pco_typed.Email
				e.As(&ee)
				if ee.IsPrimary {
					dp.Email = new(string)
					*dp.Email = ee.Address
					break
				}
			}
		}
		phones, ok := p.Relationships["phone_numbers"]
		if ok {
			for _, e := range phones {
				var phone pco_typed.Phone
				e.As(&phone)
				if phone.IsPrimary || len(phones) == 1 {
					re, _ := regexp.Compile("[^0-9]")
					phone.Number = re.ReplaceAllString(phone.Number, "")
					phone.Number = phone.Number[len(phone.Number)-10:]
					dp.Phone = new(string)
					// if phone.National != nil {
					// 	*dp.Phone = *phone.National
					// } else {
					*dp.Phone = phone.Number
					// }
					break
				}
			}
		}
		addresses, ok := p.Relationships["addresses"]
		if ok {
			for _, e := range addresses {
				var addr pco_typed.Address
				e.As(&addr)
				if addr.IsPrimary || len(addresses) == 1 {
					addr.Street = strings.TrimSpace(addr.Street)
					dp.Address = &oneChurchAddress{
						Street: addr.Street,
						City:   addr.City,
						State:  addr.State,
						Postal: addr.Zip,
					}
					dp.Address.Street = strings.ReplaceAll(dp.Address.Street, "\n", ", ")
					break
				}
			}
		}
		addHousehold(dp, p.Relationships)
		if person.Avatar != "" {
			dp.LocalImage = new(string)
			dp.Avatar = new(string)
			*dp.Avatar = person.Avatar
			ext := filepath.Ext(person.Avatar)
			*dp.LocalImage = fmt.Sprintf("img/%d%s", dp.ID, ext)
			imageUrls <- imageDownload{person.Avatar, dp}
		} else {
			imgNull.Add(1)
		}
		dpeople = append(dpeople, dp)
	}
	for k := range kids.Values {
		personsRetrieved.Add(1)
		var person pco_typed.Person
		if k.As(&person) != nil {
			return err
		}
		pid, err := strconv.Atoi(person.ID)
		if err != nil {
			return errors.Wrapf(err, "unexpected person ID: %s", person.ID)
		}
		dp := &directoryPerson{
			ID:          pid,
			FamilyName:  person.FamilyName,
			DisplayName: person.FirstName,
			Child:       true,
			Gender:      person.Gender,
		}
		if person.GivenName != "" {
			dp.FirstName = person.GivenName
		}
		addHousehold(dp, k.Relationships)
		dp.Birthday = birthdayFromDate(person.Birthdate)
		dpeople = append(dpeople, dp)
	}

	closeImg.Do(func() { close(imageUrls) })
	err = members.Err()
	if err == nil {
		err = kids.Err()
	}
	if err != nil {
		return errors.Wrap(err, "failed to retrieve directory people")
	}
	wg.Wait()

	fmt.Println("cleaning img directory")
	imgs, err := os.ReadDir(filepath.Join(cwd, "img"))
	if err != nil {
		return
	}

	for _, img := range imgs {
		base := filepath.Base(img.Name())
		base = strings.Split(base, ".")[0]
		if _, ok := currentImgEtags[base]; !ok {
			os.Remove(filepath.Join(cwd, "img", img.Name()))
		}
	}

	f, err := os.Create(filepath.Join(cwd, "people.json"))
	if err != nil {
		return
	}
	slices.SortFunc(dpeople, sortPerson)
	defer f.Close()
	e := json.NewEncoder(f)
	e.SetIndent("", "  ")
	err = e.Encode(dpeople)
	if err != nil {
		return
	}
	jsonOnce.Do(func() { jsonWritten <- true })
	return nil
}

func sortPerson(a, b *directoryPerson) int {
	return strings.Compare(a.getSortString(), b.getSortString())
}

type oneChurchAddress struct {
	Street string `json:"street"`
	City   string `json:"city"`
	State  string `json:"state"`
	Postal string `json:"postalCode"`
}
type oneChurchDate struct {
	Month int `json:"month"`
	Year  int `json:"year"`
	Day   int `json:"day"`
}
type oneChurchBirthday struct {
	oneChurchDate
	Age float64 `json:"age"`
}

func (o *oneChurchDate) Years() float64 {
	thisYear := time.Date(o.Year, time.Month(o.Month), o.Day, 0, 0, 0, 0, time.Local)
	age := float64(thisYear.Year() - o.Year)
	const yearInHours = 365 * 24
	age += time.Since(thisYear).Hours() / yearInHours
	return math.Round(age*100) / 100
}

func birthdayFromDate(d pco_typed.Date) *oneChurchBirthday {
	t := d.Time()
	if t.IsZero() {
		return nil
	}
	date := oneChurchDate{
		Year:  t.Year(),
		Month: int(t.Month()),
		Day:   t.Day(),
	}
	return &oneChurchBirthday{date, date.Years()}
}

type household struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	people []*directoryPerson

	_first string
}

func (h *household) getFirstName() string {
	if h._first == "" {
		var firsts []string
		for _, p := range h.people {
			if !p.Child {
				firsts = append(firsts, p.DisplayName)
			}
		}
		if len(firsts) == 0 {
			h._first = " "
		} else {
			sort.Strings(firsts)
			h._first = firsts[0]
		}
	}
	return h._first
}

type directoryPerson struct {
	ID          int                `json:"id"`
	FamilyName  string             `json:"-"`
	FirstName   string             `json:"-"`
	Nickname    string             `json:"-"`
	DisplayName string             `json:"name"`
	Avatar      *string            `json:"-"`
	LocalImage  *string            `json:"image_local"`
	Email       *string            `json:"email"`
	Phone       *string            `json:"phone"`
	OutOfArea   bool               `json:"out-of-area"`
	Address     *oneChurchAddress  `json:"address"`
	Birthday    *oneChurchBirthday `json:"birthday"`
	Since       *float64           `json:"since"`
	Child       bool               `json:"is_child"`
	Gender      string             `json:"gender"`
	Household   *household         `json:"household"`
}

// getSortString gives a string that will sort against any
// other person.
func (d *directoryPerson) getSortString() (value string) {
	value += d.FamilyName
	value += d.DisplayName
	if d.Household != nil {
		var bday oneChurchBirthday
		if d.Child {
			bday = *d.Birthday
		}
		value = fmt.Sprintf("%04d-%02d-%02d %s", bday.Year, bday.Month, bday.Day, value)
		value = d.Household.Name + d.Household.getFirstName() + value
	}
	return
}

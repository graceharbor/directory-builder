package internal

import (
	"bytes"
	"context"
	"os"
	"path/filepath"
	"strconv"

	"github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/model"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/types"
	"github.com/pkg/errors"
)

func Impose(ctx context.Context, cwd string) error {
	pdfPath := filepath.Join(cwd, "directory.pdf")
	f, err := os.Open(pdfPath)
	if err != nil {
		return errors.Wrapf(err, "directory not found at %s", pdfPath)
	}
	defer f.Close()

	outpath := filepath.Join(filepath.Dir(pdfPath), "directory_print.pdf")
	w, err := os.Create(outpath)
	if err != nil {
		return errors.Wrapf(err, "failed to create %s", filepath.Base(outpath))
	}
	defer w.Close()

	var conf = model.NewDefaultConfiguration()
	var nup = pdfcpu.DefaultBookletConfig()
	nup.PageSize = "Letter"
	// nup.Border = true
	// nup.BookletGuides = true
	nup.Grid = &types.Dim{
		Width:  1,
		Height: 2,
	}

	wi := bytes.NewBuffer(nil)

	err = api.Booklet(f, wi, nil, nil, nup, conf)
	if err != nil {
		return err
	}
	rr := bytes.NewReader(wi.Bytes())
	wr := bytes.NewBuffer(nil)
	err = api.Rotate(rr, wr, 90, nil, conf)
	if err != nil {
		return err
	}
	info, err := api.PDFInfo(bytes.NewReader(wr.Bytes()), filepath.Base(outpath), nil, conf)
	if err != nil {
		return err
	}
	var odds []string
	for i := 1; i < info.PageCount; i += 2 {
		odds = append(odds, strconv.Itoa(i))
	}
	err = api.Rotate(bytes.NewReader(wr.Bytes()), w, 180, odds, conf)
	return err
}

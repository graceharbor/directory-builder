package internal

import (
	"testing"

	"github.com/stretchr/testify/require"
	"golang.org/x/exp/slices"
)

// TestFamilySort ensures that families are sorted together
// by their household name, if it is given.
func TestFamilySort(t *testing.T) {
	familyToSort := &household{
		ID:   1,
		Name: "Later household",
	}
	var people = []*directoryPerson{
		{
			ID:          1,
			FamilyName:  "Later",
			FirstName:   "J",
			DisplayName: "J Later",
			Household:   familyToSort,
		},
		{
			ID:          2,
			FamilyName:  "Earlier",
			FirstName:   "B",
			DisplayName: "B Earlier",
			Household:   familyToSort,
		},
		{
			ID:          10,
			FamilyName:  "Later",
			FirstName:   "KidB",
			DisplayName: "KidB Later",
			Child:       true,
			Birthday: &oneChurchBirthday{
				oneChurchDate: oneChurchDate{
					Year:  2000,
					Month: 1,
					Day:   1,
				},
			},
			Household: familyToSort,
		},
		{
			ID:          11,
			FamilyName:  "Later",
			FirstName:   "KidA",
			DisplayName: "KidA Later",
			Child:       true,
			Birthday: &oneChurchBirthday{
				oneChurchDate: oneChurchDate{
					Year:  2001,
					Month: 1,
					Day:   1,
				},
			},
			Household: familyToSort,
		},
	}
	familyToSort.people = append(familyToSort.people, people...)
	people = append(people, &directoryPerson{
		ID:          3,
		FamilyName:  "InBetween",
		FirstName:   "J",
		DisplayName: "J InBetween",
	})
	slices.SortFunc(people, sortPerson)
	var lastNames []string
	for _, p := range people {
		lastNames = append(lastNames, p.DisplayName)
	}
	require.Equal(t, lastNames, []string{
		"J InBetween",
		"B Earlier",
		"J Later",
		"KidB Later",
		"KidA Later",
	})
}

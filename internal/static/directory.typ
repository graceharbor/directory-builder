#import "@preview/oxifmt:0.2.0": strfmt
#set page(
  width: 5.5in, height:8.5in,
  margin: (
    left: 0.25in,
    right: 0.25in,
  ),
  footer: [
    #locate(
    loc =>
    // on all except front/back covers...
    if loc.page() != 1 and loc.page() != counter(page).final(loc).at(0) {
        line(length: 100%)
        grid(
          columns: (1fr, 1fr),
          align(left, [
            #if calc.rem(loc.page(), 2) == 0 {counter(page).display("1/1", both: true)}
            #let colophon = ""
            #if "commit_hash" in sys.inputs { colophon = "via " + sys.inputs.at("commit_hash") }
            #if loc.page() + 1 == counter(page).final(loc).at(0) [
              #text(6pt)[_created: #datetime.today().display("[year]-[month]-[day]") #colophon _]
            ]
          ]),
          align(right, [
            #if calc.rem(loc.page(), 2) != 0 {counter(page).display("1/1", both: true)}
          ]),
        )
    }
  )
  ]
)

#let yamlSize = ("1":1pt)
#let yamlSizeCreation = 1pt
#for value in range(1,200) {
  yamlSize.insert(str(value), yamlSizeCreation)
  yamlSizeCreation += 1pt
}
#let stateNames = (
  "Alabama": "AL",
  "Alaska": "AK",
  "Arizona": "AZ",
  "Arkansas": "AR",
  "California": "CA",
  "Canal Zone": "CZ",
  "Colorado": "CO",
  "Connecticut": "CT",
  "Delaware": "DE",
  "District of Columbia": "DC",
  "Florida": "FL",
  "Georgia": "GA",
  "Guam": "GU",
  "Hawaii": "HI",
  "Idaho": "ID",
  "Illinois": "IL",
  "Indiana": "IN",
  "Iowa": "IA",
  "Kansas": "KS",
  "Kentucky": "KY",
  "Louisiana": "LA",
  "Maine": "ME",
  "Maryland": "MD",
  "Massachusetts": "MA",
  "Michigan": "MI",
  "Minnesota": "MN",
  "Mississippi": "MS",
  "Missouri": "MO",
  "Montana": "MT",
  "Nebraska": "NE",
  "Nevada": "NV",
  "New Hampshire": "NH",
  "New Jersey": "NJ",
  "New Mexico": "NM",
  "New York": "NY",
  "North Carolina": "NC",
  "North Dakota": "ND",
  "Ohio": "OH",
  "Oklahoma": "OK",
  "Oregon": "OR",
  "Pennsylvania": "PA",
  "Puerto Rico": "PR",
  "Rhode Island": "RI",
  "South Carolina": "SC",
  "South Dakota": "SD",
  "Tennessee": "TN",
  "Texas": "TX",
  "Utah": "UT",
  "Vermont": "VT",
  "Virgin Islands": "VI",
  "Virginia": "VA",
  "Washington": "WA",
  "West Virginia": "WV",
  "Wisconsin": "WI",
  "Wyoming": "WY",
)
#for k in stateNames.values() {
  stateNames.insert(k,k)
}


#let config = yaml("config.yaml")
#set text(font:config.style.font-family)

#image(config.logo)
#align(center,[
  #set text(24pt, weight: "bold")
  GRACE HARBOR \
  CHURCH
  #v(0.5in)
  #set text(20pt, weight: "regular")
  Member Directory \
  #config.dates
])

#pagebreak()

#let people = json("people.json")
#if config.style.member-list.visible [

#columns(config.style.member-list.columns, [
  #set text(yamlSize.at(str(config.style.member-list.font-size)))
  #for c in people {
    if (not c.is_child) [
      #c.name \
    ]
  }

])
] else [

#align(center, [ _New members in the past year (most recent in_ *bold*_)_ ])
#columns(2, [
  #set text(yamlSize.at(str(config.style.member-list.font-size)))
  #let nowmonths = datetime.today().year() * 12 + datetime.today().month()
  #for c in people {
    if (not c.is_child) and (type(c.since) != type(none)) [
      #if c.since <= 0.333 [
        *#c.name* \
      ] else if c.since < 1 [
        #c.name \
      ]
    ]
  }

])

]



#let fit_width(body) = {
  let frac = 100%
  layout(size => {
    style(styles => {
      let sizea = measure(body, styles)
      if sizea.width > size.width {
        let frac = size.width/sizea.width
        scale(x:90%, y:90%, body)
      } else {
        body
      }
    })
  })
}

#let imageWidth = 1.55in

#let values = ()
#for c in people {
  if (not c.is_child) {
    values.push(align(left)[
      #if type(c.image_local) == "string" {
        align(center, image(c.image_local, width:imageWidth, height:imageWidth, fit:"contain"))
        v(-5pt)
      }
      #text(weight:"bold", size:7.5pt)[#c.name] \
      #if type(c.phone) != type(none) [
        #c.phone.slice(0, count:3)-#c.phone.slice(3, count:3)-#c.phone.slice(6) \
      ] else [
        \
      ]
      // #fit_width(c.email) \
      #raw(c.email) \
      #if c.out-of-area [
        OUT OF AREA
        #if type(c.address) != type(none) [
          \
          #stateNames.at(c.address.state)
        ]
      ] else if type(c.address) != type(none) [
        #c.address.street \
        #c.address.city, #stateNames.at(c.address.state)  #c.address.postalCode.slice(0,5)
      ] else [
      ]
      ])
  }
}

#align(center)[
#text(7pt)[
  #grid(
    columns: (auto, auto, auto),
    rows: (2.35in),
    gutter: 4pt,
    ..values
  )
]
]

#pagebreak()

#align(center)[Kids of GHC]

#let kids = ()
#let famid = 0
#let fam = ()
#for c in people {
  if c.is_child {
    if c.household.id != famid {
      if famid != 0 {
        kids.push(fam)
      }
      fam = ([_*#text(10pt, c.household.name)*_], )
      famid = c.household.id
    }
    fam.push([])
    fam.push(text(9pt, c.name))
    if type(c.birthday) == "dictionary" {
      let bday = datetime(year: c.birthday.year, month: c.birthday.month, day:c.birthday.day)
      fam.push(text(8pt, bday.display("([month repr:short] [day padding:zero], '[year repr:last_two])")))
    } else {
      fam.push([])
    }
  }
}
#if fam.len() > 0 {kids.push(fam)}

#columns(3, gutter: 7%, [
  #for f in kids {
    block(breakable: false)[
      #f.first()
      #v(-6pt)
      #grid(
        columns:(0.6em, auto, 1fr),
        align: (left, left, right),
        row-gutter:6pt,
        ..f.slice(1)
        )
    ]
  }
])

#pagebreak()

// need to add page breaks so that the total pages is a multiple of 4
#locate(loc => {
  for i in range(4-calc.rem(loc.page(), 4)) [
    #align(center+horizon)[
      #text(8pt)[
        _this page intentionally left blank_
      ]
    ] \
    #pagebreak()
  ]
})

#align(center)[*Grace Harbor Church Covenant*]

#set text(yamlSize.at(str(config.style.covenant.font-size)))
#config.covenant

#set text(11pt)
#align(center+bottom)[_*Next members' meeting is #config.next*_]

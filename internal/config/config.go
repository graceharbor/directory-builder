package config

import (
	"bufio"
	"fmt"
	"io"
	"path/filepath"
	"strings"

	"github.com/spf13/afero"
	yaml2 "gopkg.in/yaml.v2"
	"gopkg.in/yaml.v3"
)

const currentVersion = 2
const commentWarning = `# NOTE: any comment not before the 'version' will be
#       lost during subsequent saves`

type Config = v2config

type versionedConfig struct {
	Version int `yaml:"version"`
}

func DumpConfig(fs afero.Fs, cwd string, c Config) (err error) {
	config := filepath.Join(cwd, "config.yaml")
	f, err := fs.Create(config)
	if err != nil {
		return
	}

	fmt.Fprintf(f, "%s\nversion: %d\n%s\n", c.header, currentVersion, commentWarning)

	defer f.Close()
	covenant := strings.NewReader(c.Covenant)
	c.Covenant = ""
	err = yaml2.NewEncoder(f).Encode(&c)
	fmt.Fprint(f, "covenant: |\n")

	s := bufio.NewScanner(covenant)
	for s.Scan() {
		fmt.Fprintf(f, "  %s\n", s.Text())
	}

	return
}
func LoadConfig(fs afero.Fs, cwd string) (c Config, err error) {
	config := filepath.Join(cwd, "config.yaml")
	f, err := fs.Open(config)
	if err != nil {
		return
	}
	defer f.Close()
	s := bufio.NewScanner(f)

	var header string
	for s.Scan() {
		trimmed := strings.TrimSpace(s.Text())
		if strings.HasPrefix(trimmed, "#") || trimmed == "" {
			header += trimmed + "\n"
		} else {
			break
		}
	}
	header = strings.TrimRight(header, " \n\r\t")
	f.Seek(0, io.SeekStart)
	var versionedConf versionedConfig
	err = yaml.NewDecoder(f).Decode(&versionedConf)
	if err != nil {
		return
	}

	var realConfig any
	switch versionedConf.Version {
	case 1:
		realConfig = &v1config{}
	case 2:
		realConfig = &Config{}
	default:
		err = fmt.Errorf("unsupported config version %d", versionedConf.Version)
		return
	}
	_, err = f.Seek(0, io.SeekStart)
	if err != nil {
		return
	}
	err = yaml.NewDecoder(f).Decode(realConfig)
	if err != nil {
		return
	}
	for {
		if upgrader, ok := realConfig.(upgrader); ok {
			realConfig, err = upgrader.Upgrade()
			if err != nil {
				return
			}
		} else {
			break
		}
	}
	upgraded, ok := realConfig.(*Config)
	if !ok {
		err = fmt.Errorf("internal error: did not find a *Config value")
	} else {
		c = *upgraded
	}
	c.header = header
	return
}

type upgrader interface {
	Upgrade() (any, error)
}

// v1config is for testing of the upgrade only
type v1config struct {
	PlaceGitHash bool `yaml:"place-git-hash"`
}

func (v *v1config) Upgrade() (any, error) {
	return &v2config{
		PlaceGitHash: v.PlaceGitHash,
	}, nil
}

type v2config struct {
	header string

	API struct {
		App   string `yaml:"app" label:"App ID"`
		Token string `yaml:"token" label:"Secret"`
	} `yaml:"api" label:"PCO API"`
	PlaceGitHash bool `yaml:"place-git-hash"`
	Lists        struct {
		Members int `yaml:"members"`
		Kids    int `yaml:"kids"`
	} `yaml:"lists"`
	OutOfArea *struct {
		DataLabel string `yaml:"data-label" label:"PCO data label"`
	} `yaml:"out-of-area" label:"Out of area"`
	MemberSince *struct {
		DataLabel string `yaml:"data-label" label:"PCO data label"`
	} `yaml:"member-since" label:"Induction Date"`
	HideAddress *struct {
		DataLabel string `yaml:"data-label" label:"PCO data label"`
	} `yaml:"hide-address" label:"Hide printed address"`

	Style struct {
		FontFamily string `yaml:"font-family"`
		MemberList struct {
			Visible  bool `yaml:"visible"`
			Columns  int  `yaml:"columns"`
			FontSize int  `yaml:"font-size"`
		} `yaml:"member-list"`
		Covenant struct {
			FontSize int `yaml:"font-size"`
		} `yaml:"covenant"`
	} `yaml:"style"`
	Dates    string `yaml:"dates" label:"Directory dates"`
	Next     string `yaml:"next" label:"Next meeting date"`
	Logo     string `yaml:"logo"`
	Covenant string `yaml:"covenant,omitempty"`
}

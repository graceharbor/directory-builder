package config_test

import (
	"strings"
	"testing"

	go_dedent "github.com/lithammer/dedent"
	"github.com/spf13/afero"
	"github.com/stretchr/testify/assert"
	"gitlab.com/graceharbor/directory-builder/internal/config"
)

func dedent(s string) string {
	return go_dedent.Dedent(strings.TrimPrefix(s, "\n"))
}

func TestConfigResave(t *testing.T) {
	fs := afero.NewMemMapFs()
	fs.Mkdir("cwd", 0o755)
	afero.WriteFile(fs, "cwd/config.yaml", []byte(dedent(`
		# this is my comment
		version: 1
		place-git-hash: true
		`)), 0o644)
	conf, err := config.LoadConfig(fs, "cwd")
	conf.PlaceGitHash = false
	assert.NoError(t, err)
	assert.NoError(t, config.DumpConfig(fs, "cwd", conf))
	data, err := afero.ReadFile(fs, "cwd/config.yaml")
	assert.NoError(t, err)
	prefix := dedent(`
		# this is my comment
		version: 2
		# NOTE: any comment not before the 'version' will be
		#       lost during subsequent saves
		api:
		    app: ""
		    token: ""
		place-git-hash: false
		`)
	assert.GreaterOrEqual(t, len(string(data)), len(prefix))
	assert.Equal(t, prefix, string(data)[:len(prefix)])
}

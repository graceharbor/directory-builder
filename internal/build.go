package internal

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"runtime/debug"
	"strings"

	"github.com/pdfcpu/pdfcpu/pkg/api"
	"github.com/pdfcpu/pdfcpu/pkg/pdfcpu/model"
	"github.com/spf13/afero"
	"gitlab.com/graceharbor/directory-builder/internal/config"
)

func copyConfig(src, dst string) (err error) {

	srcConfig, err := os.Open(src)
	if err != nil {
		return
	}
	defer srcConfig.Close()
	dstConfig, err := os.Create(dst)
	if err != nil {
		return
	}
	defer dstConfig.Close()

	_, err = io.Copy(dstConfig, srcConfig)
	if err != nil {
		return
	}
	return
}

func getCommit() (hash string, dirty, ok bool) {

	if info, ok := debug.ReadBuildInfo(); ok {
		for _, setting := range info.Settings {
			switch setting.Key {
			case "vcs.revision":
				hash = setting.Value
			case "vcs.modified":
				dirty = setting.Value == "true"
			}
		}
	}
	ok = len(hash) > 0
	return
}
func getShortHash() (hash string, dirty, ok bool) {
	hash, dirty, ok = getCommit()
	if ok {
		hash = hash[:8]
	}
	return
}

func GetFonts(ctx context.Context, cwd string) (fonts map[string]struct{}, err error) {
	err = os.Chdir(cwd)
	if err != nil {
		return
	}
	args := []string{"fonts"}
	cmd := exec.Command(filepath.Join(cwd, typst.Name), args...)
	var buffer strings.Builder
	cmd.Stderr = &buffer
	cmd.Stdout = &buffer
	err = cmd.Run()
	if err != nil {
		return
	}
	fonts = make(map[string]struct{})
	s := bufio.NewScanner(strings.NewReader(buffer.String()))
	for s.Scan() {
		fonts[s.Text()] = struct{}{}
	}
	return
}
func Build(ctx context.Context, cwd string) (err error) {
	fmt.Println(cwd)
	conf, err := config.LoadConfig(afero.NewOsFs(), cwd)
	if err != nil {
		return
	}

	err = os.Chdir(cwd)
	if err != nil {
		return
	}
	args := []string{"compile", "directory.typ"}
	if conf.PlaceGitHash {
		hash, dirty, ok := getShortHash()
		if !ok {
			hash = "n/a"
		} else if dirty {
			hash += "-dirty"
		}
		args = append(args, "--input", fmt.Sprintf("commit_hash=%s", hash))
	}
	cmd := exec.Command(filepath.Join(cwd, typst.Name), args...)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	err = cmd.Run()
	if err != nil {
		return
	}

	srcPDF := filepath.Join(cwd, "directory.pdf")
	outPDF := filepath.Join(cwd, "directory.embed.pdf")
	pdfconf := model.NewDefaultConfiguration()
	r, err := os.Open(srcPDF)
	if err != nil {
		fmt.Println("Error opening source PDF:", err)
		return
	}
	defer r.Close()

	// Open the output PDF file for writing
	w, err := os.Create(outPDF)
	if err != nil {
		fmt.Println("Error creating output PDF:", err)
		return
	}
	defer w.Close()

	// Embed the file into the PDF
	err = api.AddAttachments(r, w, []string{"people.json"}, false, pdfconf)
	if err != nil {
		fmt.Println("Error embedding file:", err)
		return
	}
	w.Close()
	r.Close()
	// os.Remove(srcPDF)
	// os.Rename(outPDF, srcPDF)

	// fmt.Println("File embedded successfully!")
	return
}

package internal

import (
	"bytes"
	"context"
	"crypto/sha256"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
)

func checkCtx(ctx context.Context) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
		return nil
	}
}

func Extract(ctx context.Context, cwd string) (err error) {
	fmt.Println(cwd)

	typstPath := filepath.Join(cwd, typst.Name)
	err = replaceIf(typstPath, typst.Data, 0o755)
	if err != nil {
		return err
	}
	if err := checkCtx(ctx); err != nil {
		return err
	}

	// err = replaceIf(filepath.Join(cwd, "directory.typ"), typstTemplate, 0o644)
	// if err != nil {
	// 	return err
	// }

	return nil
}

func replaceIf(path string, data []byte, perm fs.FileMode) (err error) {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, perm)
	if err != nil {
		return err
	}
	defer f.Close()
	currentData, err := io.ReadAll(f)
	if err != nil {
		return err
	}
	currentHash := sha256.Sum256(currentData)
	staticHash := sha256.Sum256(data)
	if !bytes.Equal(currentHash[:], staticHash[:]) {
		f.Seek(0, io.SeekStart)
		f.Truncate(0)
		f.Write(data)
		fmt.Printf("  dumped %s\n", filepath.Base(path))
	} else {
		fmt.Printf("  %s already up-to-date\n", filepath.Base(path))
	}
	return nil
}

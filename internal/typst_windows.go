package internal

import (
	_ "embed"
)

//go:embed static/typst_windows.exe
var typstData []byte

const typstURL = "https://github.com/typst/typst/releases/download/v0.11.0/typst-x86_64-pc-windows-msvc.zip"
const typstHash = "1d272da7bef13da3015f2c7183d7ca7729b2c53c90313dfccc168e4d52e8f561"
const typstName = "typst.exe"

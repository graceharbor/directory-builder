package internal

import (
	"os"
	"path/filepath"
)

func GetPreferredCwd() (string, error) {
	cwd, cwdErr := os.Getwd()
	if cwdErr != nil {
		return "", cwdErr
	}
	gitignoreStat, gitignoreErr := os.Stat(filepath.Join(cwd, ".gitignore"))
	buildDirStat, buildDirErr := os.Stat(filepath.Join(cwd, "build"))
	if gitignoreErr == nil && !gitignoreStat.IsDir() && buildDirErr == nil && buildDirStat.IsDir() {
		return filepath.Join(cwd, "build"), nil
	}
	return cwd, nil
}

package internal

import (
	"math"
	"sync/atomic"
)

type Progress struct {
	Total atomic.Int32
	Done  atomic.Int32
}

func (p *Progress) Fraction() float64 {
	return float64(p.Done.Load()) / float64(p.Total.Load())
}

func (p *Progress) PrecentageFloor() int {
	return int(math.Floor(p.Fraction() * 100))
}

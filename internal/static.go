package internal

import (
	"bytes"
	"crypto/sha256"
	_ "embed"
	"encoding/hex"
)

//go:embed static/directory.typ
var typstTemplate []byte

type typstMeta struct {
	Data []byte
	Name string
	URL  string
	Hash struct {
		Expected []byte
		Actual   [32]byte
	}
}

func (t *typstMeta) AssertHashesMatch() {
	if !bytes.Equal(t.Hash.Actual[:], t.Hash.Expected) {
		panic("typst binary hash does not match expected")
	}
}

var typst typstMeta

func init() {
	typst = typstMeta{
		Data: typstData,
		Name: typstName,
		URL:  typstURL,
	}
	typst.Hash.Expected, _ = hex.DecodeString(typstHash)
	typst.Hash.Actual = sha256.Sum256(typst.Data)
	typst.AssertHashesMatch()
}

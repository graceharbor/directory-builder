GHC Directory Builder
=====================

This is a program that downloads current member contact info from our
[Planning Center](https://planningcenter.com) data and builds a
printable directory (both as a plain PDF and also a half-letter sized booklet
form).  It is available as a CLI or GUI, currently only on Windows, with planned
support for Mac as well.

*Note:* There is much here that is hard-coded.  I have plans to transition that
to the user's configuration as needed.

*Note 2:* I have not yet attempted to make this clean.  This represents
simply a working version.


Building
--------

You need to download [typst](https://github.com/typst/typst/releases) for your
intended target platform and put it in `internal/static` (see
`internal/typst_<platform>.go`).

- for CLI: `go build ./cmd/directory_builder`
- for GUI: `go build ./cmd/directory_builder_gui "-H=windowsgui"`

Usage
-----

- Open the GUI application.
- Press "Select Directory" to choose the directory you want to work in.
- Press "Configure".  This currently simply *checks* that you have configuration
  already in that directory!
- Press "Extract tools" to extract **typst** and the template and logo.
- Press "Retrieve data" to download the current directory info.
- Press "Build basic directory" to run **typst** and create the plain PDF
  directory.
- Press "Impose directory for printing" to create the booklet PDF.

Configuration
-------------

The GUI doesn't yet support configuration.  You need a `config.yaml` in your
directory:

```yaml
api:
    key: your api key
    base: "https://<yoursubdomain>.onechurchsoftware.com/api"
next: "2024-04-14" # the date of your next members meeting
font-family: Calibri # a font on your system
logo: logo.png # the front cover logo ("logo.png" in your build directory gets overwritten!!!!)
dates: Feb - April 2024 # dates printed on front
covenant: |
    Your covenent that is printed on the back.
    It is multi-line and inserted directly into the typst document.
```
